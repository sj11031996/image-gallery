/** @format */

import React from "react";
import { Image } from "../../service/image/imageService";
interface ImageGalleryProps {
  image: Image;
  onClickImage: (image: Image) => void;
}
const ImageCard: React.FC<ImageGalleryProps> = ({ image, onClickImage }) => {
  return (
    <div
      className='max-w-sm rounded overflow-hidden shadow-lg'
      onClick={() => onClickImage(image)}>
      <img src={image.webformatURL} alt='' className='w-full' />
      <div className='px-6 py-4'>
        <div className='font-bold text-purple-500 text-xl mb-2'>
          Photo by {image.user}
        </div>
      </div>
    </div>
  );
};

export default ImageCard;

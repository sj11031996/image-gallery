/** @format */

import React from "react";
import { fetchImages } from "../../service/image/imageSlice";
import { useAppDispatch } from "../../service/hook";

const ImageSearch: React.FC = () => {
  const dispatch = useAppDispatch();
  const searchText = (search: string) => {
    dispatch(fetchImages(1, search));
  };
  return (
    <input
      type='text'
      placeholder='Search for an image'
      className='w-full py-2 px-4 border border-gray-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-500'
      onChange={(e) => searchText(e.target.value)}
    />
  );
};

export default ImageSearch;

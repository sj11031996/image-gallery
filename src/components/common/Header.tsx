/** @format */

import React from "react";
import ImageSearch from "./ImageSearch";
import DropdownMenu from "./DropdownMenu";

const Header: React.FC = () => {
  return (
    <header className='sticky top-0 bg-white py-4 px-6 flex items-center justify-between shadow-md'>
      <div className='flex items-center'>
        <h3 className='text-lg font-bold'>My Gallery</h3>
      </div>
      <div className='flex items-center justify-center flex-1 mx-4'>
        <ImageSearch />
      </div>
      <div>
        <DropdownMenu />
      </div>
    </header>
  );
};

export default Header;

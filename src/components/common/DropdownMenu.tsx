/** @format */

import React from "react";
import { useAppDispatch } from "../../service/hook";
import {
  setSortOrderImages,
  fetchImages,
} from "../../service/image/imageSlice";
const DropdownMenu: React.FC = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState("Default");
  const dispatch = useAppDispatch();
  const options = ["Default", "views", "size"];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionSelect = (option: string) => {
    setSelectedOption(option);
    if (option === "Default") {
      dispatch(fetchImages(1, ""));
    } else {
      dispatch(setSortOrderImages(option));
    }
    setIsOpen(false);
  };

  return (
    <div className='relative'>
      <button
        className='flex items-center justify-center p-2 rounded-md bg-gray-200 text-gray-800'
        onClick={toggleDropdown}>
        <span className='mr-2'>Sort: {selectedOption}</span>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 20 20'
          fill='currentColor'
          className={`w-4 h-4 transition-transform duration-200 transform ${
            isOpen ? "rotate-180" : ""
          }`}>
          <path
            fillRule='evenodd'
            d='M6.293 7.293a1 1 0 0 1 1.414 0L10 9.586l2.293-2.293a1 1 0 0 1 1.414 1.414l-3 3a1 1 0 0 1-1.414 0l-3-3a1 1 0 0 1 0-1.414z'
            clipRule='evenodd'
          />
        </svg>
      </button>
      {isOpen && (
        <div className='absolute top-full left-0 mt-2 w-40 rounded-md bg-white shadow-lg'>
          {options.map((option, index) => (
            <button
              key={index}
              className={`block px-4 py-2 text-gray-800 hover:bg-gray-200 w-full ${
                selectedOption === option ? "font-bold" : ""
              }`}
              onClick={() => handleOptionSelect(option)}>
              {option}
            </button>
          ))}
        </div>
      )}
    </div>
  );
};

export default DropdownMenu;

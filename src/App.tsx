/** @format */

import React from "react";
import { Route, Routes } from "react-router-dom";
import Header from "./components/common/Header";
import Footer from "./components/common/Footer";
import Home from "./page/Home";
const App: React.FC = () => {
  return (
    <>
      <div className='bg-gray-100'>
        <Header />
        <Routes>
          <Route path='/' element={<Home />} />
        </Routes>
        <Footer />
      </div>
    </>
  );
};

export default App;

/** @format */

import React from "react";
import { useAppSelector, useAppDispatch } from "../service/hook";
import { fetchImages } from "../service/image/imageSlice";
import { Image } from "../service/image/imageService";
// Define the return type of the custom hook
interface ImageListHook {
  loading: boolean;
  images: Image[];
  error: string | null;
}

const useImageList = (page: number): ImageListHook => {
  const loading = useAppSelector((state) => state.image.loading);
  const error = useAppSelector((state) => state.image.error);
  const images = useAppSelector((state) => state.image.data);
  const dispatch = useAppDispatch();
  React.useEffect(() => {
    console.log(page);
    dispatch(fetchImages(page, ""));
  }, [dispatch, page]);
  return {
    loading,
    images,
    error,
  };
};

export default useImageList;

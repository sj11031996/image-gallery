/** @format */
import { apiService } from "../apiService";
export interface Image {
  id: string;
  pageURL: string;
  type: string;
  tags: string;
  previewURL: string;
  previewWidth: number;
  previewHeight: number;
  webformatURL: string;
  webformatWidth: number;
  webformatHeight: number;
  largeImageURL: string;
  imageWidth: number;
  imageHeight: number;
  imageSize: number;
  views: number;
  downloads: number;
  collections: number;
  likes: number;
  comments: number;
  user_id: number;
  user: string;
  userImageURL: string;
}
export interface ImageResponse {
  data: Image[];
  success: boolean;
}

const apiKey = "38061510-20628f01f65bcf3ef65bc5d07";

export const getImages = async (
  page: number,
  search: string
): Promise<ImageResponse> => {
  try {
    const response: ImageResponse = await apiService.get<ImageResponse>(
      `/?key=${apiKey}&q=${search}&image_type=photo&pretty=true&page=${page}&per_page=20`
    );
    return response;
  } catch (error: any) {
    throw new Error(
      error.response?.data?.message || "Failed to fetch products"
    );
  }
};

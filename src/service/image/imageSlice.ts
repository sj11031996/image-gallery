/** @format */

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "../store";
import { Image, getImages } from "./imageService";

interface ImageState {
  data: Image[];
  loading: boolean;
  error: string | null;
}

const initialState: ImageState = {
  data: [],
  loading: false,
  error: null,
};

const imageSlice = createSlice({
  name: "image",
  initialState,
  reducers: {
    setImages: (state, action: PayloadAction<Image[]>) => {
      state.data = action.payload;
    },
    setSearchImages: (state, action: PayloadAction<Image[]>) => {
      state.data = action.payload;
    },
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    setError: (state, action: PayloadAction<string | null>) => {
      state.error = action.payload;
    },
    setSortOrderImages: (state, action: PayloadAction<string>) => {
      if (action.payload === "views") {
        state.data = state.data.sort((a, b) => a.views - b.views);
      } else if (action.payload === "size") {
        state.data = state.data.sort((a, b) => a.imageSize - b.imageSize);
      }
    },
  },
});

export const {
  setImages,
  setSearchImages,
  setLoading,
  setError,
  setSortOrderImages,
} = imageSlice.actions;

// Thunk action to fetch images
export const fetchImages =
  (page: number, search: string): AppThunk =>
  async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const images: any = await getImages(page, search);
      dispatch(setImages(images));
      setTimeout(() => {
        dispatch(setLoading(false));
      }, 1000);
    } catch (error: any) {
      dispatch(setError(error.message));
      dispatch(setLoading(false));
    }
  };
// Thunk action to fetch search images
export default imageSlice.reducer;

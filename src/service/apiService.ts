/** @format */

const BASE_URL = "https://pixabay.com/api";

interface ApiResponse<T> {
  hits: T;
}

export const apiService = {
  get: async <T>(endpoint: string): Promise<T> => {
    try {
      const response = await fetch(`${BASE_URL}${endpoint}`);
      if (!response.ok) {
        throw new Error("Request failed");
      }
      const data: ApiResponse<T> = await response.json();
      return data.hits;
    } catch (error: any) {
      throw new Error(`Error: ${error.message}`);
    }
  },
};

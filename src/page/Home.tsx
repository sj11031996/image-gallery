/** @format */

import React from "react";
import SkeletonLoader from "../components/common/SkeletonLoader";
import useImageList from "../hooks/useImageList";
import { Image } from "../service/image/imageService";
import ImageCard from "../components/layouts/ImageCard";
import { debounce } from "lodash";
const Home: React.FC = () => {
  const [page, setPage] = React.useState(1);
  const { loading, images, error } = useImageList(page);
  const [selectedImage, setSelectedImage] = React.useState<Image | null>(null);
  // Function to handle scrolling
  const handleScroll = debounce(() => {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement;
    if (scrollTop + clientHeight >= scrollHeight - 20 && !loading) {
      // User has scrolled to the bottom, not loading, and there are more products to fetch
      setPage((prevPage) => prevPage + 1);
      window.scrollTo({
        top: 0,
        behavior: "smooth", // Scroll smoothly to the top
      });
    }
  }, 200);

  // Attach scroll event listener when the component mounts
  React.useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [handleScroll]);

  const handleImageClick = React.useCallback((image: Image) => {
    setSelectedImage(image);
    window.scrollTo({
      top: 0,
      behavior: "smooth", // Scroll smoothly to the top
    });
  }, []);
  if (loading) {
    return <SkeletonLoader />;
  }
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div className='container mx-auto py-6'>
      <div className={`${selectedImage ? "flex" : ""}`}>
        <div className={`grid grid-cols-${selectedImage ? 2 : 4} gap-4`}>
          {images?.length > 0 &&
            images.map((image: Image, i) => (
              <ImageCard
                key={i}
                image={image}
                onClickImage={handleImageClick}
              />
            ))}
        </div>
        {selectedImage && (
          <div className='flex-1 mt-0 ml-3'>
            <div className='bg-white rounded-lg shadow-md p-4'>
              <div className='flex justify-between items-center'>
                <h3 className='text-lg font-bold'>selectedImage.user</h3>
                <button
                  className='text-gray-500 hover:text-gray-700'
                  onClick={() => setSelectedImage(null)}>
                  <svg
                    className='w-5 h-5'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'>
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M6 18L18 6M6 6l12 12'></path>
                  </svg>
                </button>
              </div>
              <div className='rounded'>
                <img
                  src={selectedImage.webformatURL}
                  alt={selectedImage.user}
                  className='max-h-full object-cover'
                />
                <div className='px-6 py-4'>
                  <div className='font-bold text-purple-500 text-xl mb-2'>
                    Photo by {selectedImage.user}
                  </div>
                  <ul>
                    <li>
                      <strong>Views: </strong>
                      {selectedImage.views}
                    </li>
                    <li>
                      <strong>Downloads: </strong>
                      {selectedImage.downloads}
                    </li>
                    <li>
                      <strong>Likes: </strong>
                      {selectedImage.likes}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Home;
